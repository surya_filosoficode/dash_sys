const mongoModel = require('../../lib/mongo/conn_mongo')



exports.check_data = (mongoPath, collection, mongoSchema, where, resolve) => {
    var method = ": mongo: check_data: ";
    // return new Promise(resolve => {
        var mongoModels = mongoModel.mongoModel(mongoPath, collection, mongoSchema)

        mongoModels.find(where, function (err, docs) {
            // console.log(docs)
            var return_tmp = {"sts": false, "msg": err, "data":{}}
            
            if (!err) {
                // if (docs.length == 0)
                return_tmp = {"sts": true, "msg": "success", "data":docs}
            }else{
                console.log(mongoPath+method+"error: "+ err)
            }
            // console.log(docs)
            resolve(return_tmp)
        });
    // })
}

exports.save_mongo = (mongoPath, collection, mongoSchema, data, resolve) => {
    var method = ": mongo: save_mongo: ";
    
    var mongoModels = mongoModel.mongoModel(mongoPath, collection, mongoSchema)
    var msg = new mongoModels(data)
    msg.save().
        then(doc => {
            // console.log(TAG+method+"success: "+doc.id)
            resolve({sts:true, msg:doc.id})
        }).catch(err => {
            // console.log(TAG+method+"error: "+ err)
            resolve({sts:true, msg:err})
        })
}

exports.update_mongo = (mongoPath, collection, mongoSchema, where, data, resolve) => {
    // return new Promise(resolve => {
        var mongoModels = mongoModel.mongoModel(mongoPath, collection, mongoSchema.DEFAULT_SCHEMA)

        // console.log(data.data)
        mongoModels.findOneAndUpdate(where, data, function(err, docs){
            // console.log(do)

            resolve(docs)
        })

        
}


const save_all_mongo = (mongoPath, collection, mongoSchema, data, resolve) => {
    var method = ": mongo: save_all_mongo: ";
    
    var mongoModels = mongoModel.mongoModel(mongoPath, collection, mongoSchema)
    var msg = new mongoModels(data)
    msg.save().
        then(doc => {
            console.log(TAG+method+"success: "+doc.id)
            resolve({sts:true, msg:doc.id})
        }).catch(err => {
            console.log(TAG+method+"error: "+ err)
            resolve({sts:true, msg:err})
        })
}

const save_sum_mongo = (data) => {
    var method = ": mongo: data_sum: ";
    return new Promise(resolve => {
        var collection = MongoPath.CL_PAJAK_SUM

        var arr_data = {
            "sources":"http://simpeg.malangkota.go.id/serv/get.php",
            "methode":"get",
            "status":true,
            "status_msg":"success",
            "time_ex": date.get_date_time(),
            "periode": date.get_date(),
            "data":data
        }

        var mongoModels = mongoModel.mongoModel(mongoPath, collection, mongoSchema.DEFAULT_SCHEMA)

        var msg = new mongoModels(arr_data)

        msg.save().
            then(doc => {
                console.log(TAG+method+"success: "+doc.id)
            }).catch(err => {
                console.log(TAG+method+"error: "+ err)
            })
        resolve(msg)
    })
}



const update_all_mongo = (data) => {
    var method = ": mongo: update_all_mongo: ";
    return new Promise(resolve => {
        var collection = MongoPath.CL_PAJAK_ALL

        var arr_data = {
            "sources":"http://simpeg.malangkota.go.id/serv/get.php",
            "methode":"get",
            "status":true,
            "status_msg":"success",
            "time_ex": date.get_date_time(),
            "data":data
        }

        var mongoModels = mongoModel.mongoModel(mongoPath, collection, mongoSchema.DEFAULT_SCHEMA)


        const where = {periode: date.get_date()};
        mongoModels.findOneAndUpdate(where, arr_data, function(err, docs){
            // console.log(docs);
            var log_msg = TAG+method+"error: "+err;
            var return_tmp = {"sts": false, "msg": err}
            
            if (!err) {
                if (docs)
                    return_tmp = {"sts": true, "msg": "success"}

                    log_msg = TAG+method+"success: 1";
            }
            
            console.log(log_msg)

            resolve(return_tmp)
        })
    })
}

const update_sum_mongo = (data) => {
    var method = ": mongo: update_sum_mongo: ";
    return new Promise(resolve => {
        var collection = MongoPath.CL_PAJAK_SUM

        var arr_data = {
            "sources":"http://simpeg.malangkota.go.id/serv/get.php",
            "methode":"get",
            "status":true,
            "status_msg":"success",
            "time_ex": date.get_date_time(),
            "data":data
        }

        var mongoModels = mongoModel.mongoModel(mongoPath, collection, mongoSchema.DEFAULT_SCHEMA)


        const where = {periode: date.get_date()};
        mongoModels.findOneAndUpdate(where, arr_data, function(err, docs){
            // console.log(docs);
            var log_msg = TAG+method+"error: "+err;
            var return_tmp = {"sts": false, "msg": err}
            
            if (!err) {
                if (docs)
                    return_tmp = {"sts": true, "msg": "success"}

                    log_msg = TAG+method+"success: 1";
            }
            
            console.log(log_msg)

            resolve(return_tmp)
        })
    })
}
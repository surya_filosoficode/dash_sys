/**
 * @author surya_hanggara, yoga_utama_putra
 * @email suryahanggara@gmail.com
 * @create date 2021-02-18 11:03:17
 * @modify date 2021-05-04 13:43:38
 * @desc [library for generate date values]
 */

// global lib


// local lib
const {get_data_basic_auth} = require('../../lib/axios/MainAxios')
const date = require('../../lib/datetime/date')
const mongoModel = require("../../lib/mongo/conn_mongo")

// variable
const ListApiUrl = require('../../var/app_dash/ListApiUrl')
const MongoPath = require('../../var/app_dash/MongoPath')
const mongoSchema = require("../../var/mongo_schemas/SambatSchema")
const {SAMBAT_BASIC_AUTH} = require("../../var/app_dash/BasicAuthParam")

const TAG = "SAMBAT_FULL_MONGO"

const mongoBase = MongoPath.PATH_SAMBAT

console.time("run-apps")


exports.set_data = () => {
    var method = ": mongo: set_data: ";
    // console.log(ListApiUrl.URL_PARAM_FULL)
    var data = get_data_basic_auth(ListApiUrl.URL_PARAM_FULL, SAMBAT_BASIC_AUTH, callback =>{
        main_data = callback
        
        var where = {periode:date.get_date()}

        var arr_data = {
            "sources":ListApiUrl.URL_PARAM_FULL,
            "methode":"get",
            "status":true,
            "status_msg":"success",
            "time_ex": date.get_date_time(),
            "periode": date.get_date(),
            "tipe": "full",
            "data":main_data
        }

        var mongoModels = mongoModel.mongoModel(mongoBase, MongoPath.CL_SAMBAT_FULL, mongoSchema.SAMBAT_SCHEMA)
        mongoModels.find(where, function (err, docs) {
            if(docs.length == 0){
                var method = ": insert_seris: "

                var msg = new mongoModels(arr_data)
                msg.save().
                    then(doc => {
                        console.log(TAG+method+" true")
                    }).catch(err => {
                        console.log(TAG+method+" false: "+err)
                    })
            }else{
                mongoModels.findOneAndUpdate(where, arr_data, function(err, docs){
                    var method = ": update_seris: "
                    var return_tmp = {"sts": false, "msg": err, "data":{}}
                    if(!err){
                        console.log(TAG+method+" true")
                        return_tmp = {"sts": true, "msg": "success", "data":docs}
                    }else{
                        console.log(TAG+method+" false: "+err)
                    }
                })
            }
        })

    })
}




// this.set_data()

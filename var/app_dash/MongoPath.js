exports.PATH_PAJAK = 'mongodb://localhost:27017/dash_pajak'
    exports.CL_PAJAK_ALL = "pajak_mains"
    exports.CL_PAJAK_SUM = "pajak_sums"
    

exports.PATH_KEPEGAWAIAN = 'mongodb://localhost:27017/dash_kepegawaian'
    exports.CL_KEPEG_CUR = "kepegawaian_cur"
    exports.CL_KEPEG_SERIS = "kepegawaian_seris"


exports.PATH_PDAM = 'mongodb://localhost:27017/dash_pdam'
    exports.CL_PDAM_PELANGGAN_SERIS = "data_pelanggan_seris"
    exports.CL_PDAM_ADUAN_SERIS     = "data_aduan_seris"
    exports.CL_PDAM_FTAB_MAIN       = "data_fountentab_main"
    exports.CL_PDAM_FTAB_SERIS      = "data_fountentab_seris"


exports.PATH_SAMBAT = 'mongodb://localhost:27017/dash_sambat'
    exports.CL_SAMBAT_ACC_MASUK = "sm_acc_masuk"
    exports.CL_SAMBAT_ADUAN_MASUK = "sm_aduan_masuk"
    exports.CL_SAMBAT_FULL = "sm_full"
    exports.CL_SAMBAT_JENIS = "sm_jn_kate"
    exports.CL_SAMBAT_SKPD = "sm_skpd"
    exports.CL_SAMBAT_TANGGAPAN = "sm_tanggapan"


exports.PATH_SPM = 'mongodb://localhost:27017/dash_spm'
    exports.CL_SPM_SERIS_DN = "dn_data_seris"
    exports.CL_SPM_CUR_DN = "dn_data_mains"
    exports.CL_SPM_SERIS_P = "p_data_seris"
    exports.CL_SPM_CUR_P = "p_data_mains"
    exports.CL_SPM_SERIS_PV = "pv_data_seris"
    exports.CL_SPM_CUR_PV = "pv_data_mains"
    exports.CL_SPM_PNY = "pny_data_mains"
    exports.CL_SPM_RS = "rs_data_mains"


exports.PATH_PKK = 'mongodb://localhost:27017/dash_pkk'
    exports.CL_PKK_RT = "sum_rt_kel"
    exports.CL_PKK_RW = "sum_rw_kel"
    exports.CL_PKK_PY = "sum_py_kel"
    exports.CL_PKK_KP = "sum_kp_kel"
    exports.CL_PKK_DW = "sum_dw_kel"
    exports.CL_PKK_ANG_DW = "sum_ang_dw_kel"



exports.PATH_PENDIDIKAN = 'mongodb://localhost:27017/dash_pendidikan'
    exports.CL_PESERTA_DIDIK = "peserta_seris"
    exports.CL_PENDIDIK = "pendidik_seris"
    exports.CL_MASTER_SKLH = "master_sklh"
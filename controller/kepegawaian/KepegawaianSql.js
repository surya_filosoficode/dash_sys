/**
 * @author surya_hanggara, yoga_utama_putra
 * @email suryahanggara@gmail.com
 * @create date 2021-02-18 11:03:17
 * @modify date 2021-05-06 08:59:26
 * @desc [library for generate date values]
 */

// global lib


// local lib
const {get_data_basic_auth} = require('../../lib/axios/MainAxios')
const date = require('../../lib/datetime/date')
const MysqlDbParam = require("../../var/app_dash/MysqlDbParam")
const ExtnKepegawaian = require("./ExtnKepegawaian")

// variable
const ListApiUrl = require('../../var/app_dash/ListApiUrl')
const BasicAuthParam = require("../../var/app_dash/BasicAuthParam")

const TAG = "PAJAK_SQL"
var date_in = date.date_full

const CUR_ALL = MysqlDbParam.KAPEG_CUR_ALL
const CUR_SUM = MysqlDbParam.KAPEG_CUR_SUM

const CUR_STATUS = MysqlDbParam.KAPEG_CUR_STATUS
const CUR_JK = MysqlDbParam.KAPEG_CUR_JK
const CUR_GOL_DETAIL = MysqlDbParam.KAPEG_CUR_GOL_DETAIL
const CUR_GOL = MysqlDbParam.KAPEG_CUR_GOL
const CUR_AGAMA = MysqlDbParam.KAPEG_CUR_AGAMA


console.time("run-apps")

// console.log(ListApiUrl.API_KEPEGAWAIAN)
exports.set_data = () => {
    var method = ": set_data: ";

    var data = get_data_basic_auth(ListApiUrl.API_KEPEGAWAIAN, BasicAuthParam.KEPEG_BASIC_AUTH, async (callback)=>{
        // console.log(callback)
        var main_data = []

        var arr_agama = []
        var arr_t = []
        var arr_sts = []
        var arr_jk = []
        var arr_gol = []
        var arr_gol_d = []
        var arr_t_seris = []
        
        if(callback.sts){
            main_data = callback.data.var_item

            // ----------------------logic data processing-------------------------
            var row = 1

            var all_pegawai = 0
            for (let i in main_data) {
                // console.log(main_data[i])
                var jumlah_total = parseInt(main_data[i].jumlah_total)

                all_pegawai += jumlah_total

                // --------------------------- current --------------------------------
                    arr_t.push([`${(main_data[i].unit_kerja).replace(/ /g, "")}`,
                                `${main_data[i].unit_kerja}`,
                                `${main_data[i].jumlah_total}`]) // data
                // --------------------------- current --------------------------------

                // --------------------------- agama --------------------------------
                    arr_agama.push([`${i}_agama_islam`, `${main_data[i].unit_kerja}`, `agama_islam`, `${main_data[i].agama_islam}`])
                    arr_agama.push([`${i}_agama_kristen`, `${main_data[i].unit_kerja}`, `agama_kristen`, `${main_data[i].agama_kristen}`])
                    arr_agama.push([`${i}_agama_katholik`, `${main_data[i].unit_kerja}`, `agama_katholik`, `${main_data[i].agama_katholik}`])
                    arr_agama.push([`${i}_agama_hindu`, `${main_data[i].unit_kerja}`, `agama_hindu`, `${main_data[i].agama_hindu}`])
                    arr_agama.push([`${i}_agama_budha`, `${main_data[i].unit_kerja}`, `agama_budha`, `${main_data[i].agama_budha}`])
                // --------------------------- agama --------------------------------

                // --------------------------- golongan --------------------------------
                    arr_gol.push([`${i}_golongan_I`, `${(main_data[i].unit_kerja).replace(/ /g, "")}`, `golongan_I`, `${main_data[i].jumlah_golongan_I}`])
                    arr_gol.push([`${i}_golongan_II`, `${(main_data[i].unit_kerja).replace(/ /g, "")}`, `golongan_II`, `${main_data[i].jumlah_golongan_II}`])
                    arr_gol.push([`${i}_golongan_III`, `${(main_data[i].unit_kerja).replace(/ /g, "")}`, `golongan_III`, `${main_data[i].jumlah_golongan_III}`])
                    arr_gol.push([`${i}_golongan_IV`, `${(main_data[i].unit_kerja).replace(/ /g, "")}`, `golongan_IV`, `${main_data[i].jumlah_golongan_IV}`])
                // --------------------------- golongan --------------------------------

                // --------------------------- detail golongan --------------------------------
                    arr_gol_d.push([`${i}_d_golongan_Ia`, `${i}_golongan_I`, `${(main_data[i].unit_kerja).replace(/ /g, "")}`, `A`, `${main_data[i].golongan_Ia}`])
                    arr_gol_d.push([`${i}_d_golongan_Ib`, `${i}_golongan_I`, `${(main_data[i].unit_kerja).replace(/ /g, "")}`, `B`, `${main_data[i].golongan_Ib}`])
                    arr_gol_d.push([`${i}_d_golongan_Ic`, `${i}_golongan_I`, `${(main_data[i].unit_kerja).replace(/ /g, "")}`, `C`, `${main_data[i].golongan_Ic}`])
                    arr_gol_d.push([`${i}_d_golongan_Id`, `${i}_golongan_I`, `${(main_data[i].unit_kerja).replace(/ /g, "")}`, `D`, `${main_data[i].golongan_Id}`])
                    arr_gol_d.push([`${i}_d_golongan_IIa`, `${i}_golongan_II`, `${(main_data[i].unit_kerja).replace(/ /g, "")}`, `A`, `${main_data[i].golongan_IIa}`])
                    arr_gol_d.push([`${i}_d_golongan_IIb`, `${i}_golongan_II`, `${(main_data[i].unit_kerja).replace(/ /g, "")}`, `B`, `${main_data[i].golongan_IIb}`])
                    arr_gol_d.push([`${i}_d_golongan_IIc`, `${i}_golongan_II`, `${(main_data[i].unit_kerja).replace(/ /g, "")}`, `C`, `${main_data[i].golongan_IIc}`])
                    arr_gol_d.push([`${i}_d_golongan_IId`, `${i}_golongan_II`, `${(main_data[i].unit_kerja).replace(/ /g, "")}`, `D`, `${main_data[i].golongan_IId}`])
                    arr_gol_d.push([`${i}_d_golongan_IIIa`, `${i}_golongan_III`, `${(main_data[i].unit_kerja).replace(/ /g, "")}`, `A`, `${main_data[i].golongan_IIIa}`])
                    arr_gol_d.push([`${i}_d_golongan_IIIb`, `${i}_golongan_III`, `${(main_data[i].unit_kerja).replace(/ /g, "")}`, `B`, `${main_data[i].golongan_IIIb}`])
                    arr_gol_d.push([`${i}_d_golongan_IIIc`, `${i}_golongan_III`, `${(main_data[i].unit_kerja).replace(/ /g, "")}`, `C`, `${main_data[i].golongan_IIIc}`])
                    arr_gol_d.push([`${i}_d_golongan_IIId`, `${i}_golongan_III`, `${(main_data[i].unit_kerja).replace(/ /g, "")}`, `D`, `${main_data[i].golongan_IIId}`])
                    arr_gol_d.push([`${i}_d_golongan_IVa`, `${i}_golongan_IV`, `${(main_data[i].unit_kerja).replace(/ /g, "")}`, `A`, `${main_data[i].golongan_IVa}`])
                    arr_gol_d.push([`${i}_d_golongan_IVb`, `${i}_golongan_IV`, `${(main_data[i].unit_kerja).replace(/ /g, "")}`, `B`, `${main_data[i].golongan_IVb}`])
                    arr_gol_d.push([`${i}_d_golongan_IVc`, `${i}_golongan_IV`, `${(main_data[i].unit_kerja).replace(/ /g, "")}`, `C`, `${main_data[i].golongan_IVc}`])
                    arr_gol_d.push([`${i}_d_golongan_IVd`, `${i}_golongan_IV`, `${(main_data[i].unit_kerja).replace(/ /g, "")}`, `D`, `${main_data[i].golongan_IVd}`])
                // --------------------------- detail golongan --------------------------------

                // --------------------------- sts_nikah --------------------------------
                    arr_sts.push([`${i}_status_kawin`, `${(main_data[i].unit_kerja).replace(/ /g, "")}`, `status_kawin`, `${main_data[i].status_kawin}`])
                    arr_sts.push([`${i}_status_belum_kawin`, `${(main_data[i].unit_kerja).replace(/ /g, "")}`, `status_belum_kawin`, `${main_data[i].status_belum_kawin}`])
                // --------------------------- sts_nikah --------------------------------

                // --------------------------- jenis_kelamin --------------------------------
                    arr_jk.push([`${i}_laki`, `${(main_data[i].unit_kerja).replace(/ /g, "")}`, `laki`, `${main_data[i].laki}`])
                    arr_jk.push([`${i}_perempuan`, `${(main_data[i].unit_kerja).replace(/ /g, "")}`, `perempuan`, `${main_data[i].perempuan}`])
                // --------------------------- jenis_kelamin --------------------------------
                
                // --------------------------- t_seris --------------------------------
                    arr_t_seris.push([`${(main_data[i].unit_kerja).replace(/ /g, "")}`, `${main_data[i].unit_kerja}`, `${date_in}`, `${main_data[i].jumlah_total}`])
                // --------------------------- t_seris --------------------------------
                // console.log(arr_t_seris)
                row++
            }

            // console.log(arr_gol_d)
            // ----------------------logic data processing-------------------------

            await Promise.all(
                [
                    ExtnKepegawaian.insert_data(CUR_AGAMA, arr_agama),
                    ExtnKepegawaian.insert_data(CUR_JK, arr_jk),
                    ExtnKepegawaian.insert_data(CUR_STATUS, arr_sts),
                    ExtnKepegawaian.insert_data(CUR_GOL, arr_gol),
                    ExtnKepegawaian.insert_data(CUR_GOL_DETAIL, arr_gol_d),
                    ExtnKepegawaian.insert_data(CUR_SUM, arr_t),
                    ExtnKepegawaian.insert_data_seris(CUR_ALL, arr_t_seris)
                ]
                ).then((values) => {
                // console.log(values);
                console.timeEnd("run-apps")
              });
            
        }

    })




}


// this.set_data()
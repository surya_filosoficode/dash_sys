/**
 * @author surya_hanggara, yoga_utama_putra
 * @email suryahanggara@gmail.com
 * @create date 2021-02-18 11:03:17
 * @modify date 2021-06-11 14:38:58
 * @desc [library for generate date values]
 */

// global lib


// local lib
const {post_data_basic_auth, get_data_basic_auth} = require('../../lib/axios/MainAxios')
const date = require('../../lib/datetime/date')
const MysqlDbParam = require("../../var/app_dash/MysqlDbParam")
const {PDAM_BASIC_AUTH} = require("../../var/app_dash/BasicAuthParam")
const ExtnPdam = require("./ExtnPdam")

// variable
const ListApiUrl = require('../../var/app_dash/ListApiUrl')

const TAG = "PDAM_ADUAN_REKAP_SQL"
var date_in = date.date_full

const DB_NAME = MysqlDbParam.PDAM_DB_NAME
var TABLE_NAME = MysqlDbParam.PDAM_SERIS_ADUAN

console.time("run-apps")

// console.log(ListApiUrl.API_KEPEGAWAIAN)
exports.set_data = () => {
    var method = ": set_data: ";
    const post_data = {'jenis':'rekap'}
    var data = post_data_basic_auth(ListApiUrl.API_PDAM_ADUAN_REKAP, PDAM_BASIC_AUTH, post_data, async (callback)=>{
        console.log(callback)

        var th = ""
        var m = ""
        var d = ""

        var d_data = callback.data.data
        

        // process.exit()
        var main_data = []

        var arr_list = []
        
        var str_dates = "0000-00-00"
        if(callback.sts){
            main_data = d_data.pengaduan
            var data_periode = d_data.periode

            th = data_periode.substr(0, 4)
            m = data_periode.substr(4, 2)
            d = date.get_single_date

            str_dates = th+"-"+m+"-"+d

            // ----------------------logic data processing-------------------------
            var row = 1

            for (let i in main_data) {
                for (let key in main_data[i]) {
                    var tmp = [`${date_in}_${i}`,
                        `${str_dates}`,
                        `${key}`,
                        `${main_data[i][key]}`
                    ]
                    
                    arr_list.push(tmp)
                    // console.log(main_data[i][key])
                    row++
                }
            }

            console.log(arr_list)

            // process.exit()
            // ----------------------logic data processing-------------------------

            await Promise.all(
                    [
                        ExtnPdam.insert_data_seris(DB_NAME, TABLE_NAME, arr_list)
                    ]
                ).then((values) => {
                    console.log(TAG+method+" method: end of run")
                    console.timeEnd("run-apps")
                });
        }

    })




}


// this.set_data()
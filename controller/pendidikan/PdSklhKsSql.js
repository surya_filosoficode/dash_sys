/**
 * @author surya_hanggara, yoga_utama_putra
 * @email suryahanggara@gmail.com
 * @create date 2021-02-18 11:03:17
 * @modify date 2021-06-11 14:38:58
 * @desc [library for generate date values]
 */

// global lib


// local lib
const {post_data_basic_auth, get_data_basic_auth} = require('../../lib/axios/MainAxios')
const date = require('../../lib/datetime/date')
const MysqlDbParam = require("../../var/app_dash/MysqlDbParam")
const {PDD_BASIC_AUTH} = require("../../var/app_dash/BasicAuthParam")
const ExtnPdam = require("./ExtnPendidikan")

// variable
const ListApiUrl = require('../../var/app_dash/ListApiUrl')

const TAG = "PENDIDIKAN_SEKOLAH_KHUSUS_SQL"
var date_in = date.date_full

const DB_NAME = MysqlDbParam.PENDIDIKAN_DB_NAME
var TABLE_NAME = MysqlDbParam.PD_MASTER_SKLH

console.time("run-apps")
const tipe = "ks"

// console.log(ListApiUrl.API_KEPEGAWAIAN)
exports.set_data = () => {
    var method = ": set_data: ";
    const post_data = {token:'67A84C2614290BAEEBC797C2822EF6480E38EEC0137812C7A890C9B97321DFF9EEE49F1A56D8FB3DDF6F58F28F530BA8D92E434D1C7B9242FB75FF29E7D18318'}
    
    var data = post_data_basic_auth(ListApiUrl.API_PD_MS_KHUSUS, PDD_BASIC_AUTH, post_data, async (callback)=>{
        // console.log(callback)
        // process.exit()

        const str_dates = date.get_date()

        const sts_req = callback.sts
        const msg_req = callback.msg  
        
        var arr_list = []

        if(sts_req){
            const data_req = callback.data
            const title = data_req.title
            const item = data_req.item
            
            // ----------------------logic data processing-------------------------
            var row = 1

            for (let i in item) {

                var kode_wilayah = ""
                var nama = ""
                var slbn = ""
                var slbs = ""
                var sdlbn = ""
                var sdlbs = ""
                var smplbn = ""
                var smplbs = ""
                var smlbn = ""
                var smlbs = ""

                // console.log(item[i])
                // process.exit()
                if(item[i]){
                    // console.log("ok")
                    kode_wilayah = item[i].kode_wilayah
                    nama = item[i].nama
                    slbn = item[i].slbn
                    slbs = item[i].slbs
                    sdlbn = item[i].sdlbn
                    sdlbs = item[i].sdlbs
                    smplbn = item[i].smplbn
                    smplbs = item[i].smplbs
                    smlbn = item[i].smlbn
                    smlbs = item[i].smlbs

                    
                    arr_list.push(["", date_in, parseInt(kode_wilayah).toString(), nama, "SEKOLAH LUAR BIASA NEGERI", tipe, slbn.toString()])
                    arr_list.push(["", date_in, parseInt(kode_wilayah).toString(), nama, "SEKOLAH LUAR BIASA SWASTA", tipe, slbs.toString()])
                    arr_list.push(["", date_in, parseInt(kode_wilayah).toString(), nama, "SD LUAR BIASA NEGERI", tipe, sdlbn.toString()])
                    arr_list.push(["", date_in, parseInt(kode_wilayah).toString(), nama, "SD LUAR BIASA SWASTA", tipe, sdlbs.toString()])
                    arr_list.push(["", date_in, parseInt(kode_wilayah).toString(), nama, "SMP LUAR BIASA NEGERI", tipe, smplbn.toString()])
                    arr_list.push(["", date_in, parseInt(kode_wilayah).toString(), nama, "SMP LUAR BIASA SWASTA", tipe, smplbs.toString()])
                    arr_list.push(["", date_in, parseInt(kode_wilayah).toString(), nama, "SMA LUAR BIASA NEGERI", tipe, smlbn.toString()])
                    arr_list.push(["", date_in, parseInt(kode_wilayah).toString(), nama, "SMA LUAR BIASA SWASTA", tipe, smlbs.toString()])

                    // console.log(tmp)
                }
            }

            // console.log(arr_list)
            // process.exit()
            // ----------------------logic data processing-------------------------

            await Promise.all(
                    [
                        ExtnPdam.insert_data_seris_tipe(DB_NAME, TABLE_NAME, tipe, arr_list)
                    ]
                ).then((values) => {
                    console.log(TAG+method+" method: end of run")
                    console.timeEnd("run-apps")
                });
        }

    })




}


// this.set_data()
/**
 * @author surya_hanggara, yoga_utama_putra
 * @email suryahanggara@gmail.com
 * @create date 2021-02-18 11:03:17
 * @modify date 2021-05-05 12:18:20
 * @desc [library for generate date values]
 */

// spm
const SpmDnMg = require("../controller/spm/SpmDnMg")
const SpmDnSql = require("../controller/spm/SpmDnSql")

const SpmPMg = require("../controller/spm/SpmPMg")
const SpmPSql = require("../controller/spm/SpmPSql")

const SpmPnyMg = require("../controller/spm/SpmPnyMg")
const SpmPnySql = require("../controller/spm/SpmPnySql")

const SpmPvMg = require("../controller/spm/SpmPvMg")
const SpmPvSql = require("../controller/spm/SpmPvSql")

const SpmRsMg = require("../controller/spm/SpmRsMg")
const SpmRsSql = require("../controller/spm/SpmRsSql")


const runApps = () =>{
    SpmDnMg.set_data()
    SpmDnSql.set_data()

    SpmPMg.set_data()
    SpmPSql.set_data()

    SpmPnyMg.set_data()
    SpmPnySql.set_data()

    SpmPvMg.set_data()
    SpmPvSql.set_data()

    SpmRsMg.set_data()
    SpmRsSql.set_data()
}

runApps()
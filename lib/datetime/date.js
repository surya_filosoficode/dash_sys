/**
 * @author surya_hanggara, yoga_utama_putra
 * @email suryahanggara@gmail.com
 * @create date 2021-02-18 11:03:17
 * @modify date 2021-05-04 10:13:10
 * @desc [library for generate date values]
 */

let datetime = new Date()

var list_of_day = {
  "en":["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
  "id":["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"]
}

var list_of_month = {
  "en":["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
  "id":["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"]
}


exports.date_full = datetime.toISOString().slice(0, 10)

// console.log(datetime)

// current Date
exports.date = datetime.getDate()
// current hours
exports.hours = datetime.getHours()
// current minutes
exports.minutes = datetime.getMinutes()
// current Month
exports.month = datetime.getMonth() + 1
// current Years
exports.year = datetime.getFullYear()


/**
description: function get full date
call: get_date() ==> for get yyyy-mm-dd,
      get_date_time() ==> for get yyyy-mm-dd H:i:s,
*/
  // get full date
  exports.get_date = () => {
    d = datetime.getDate()
    m = datetime.getMonth()
    y = datetime.getFullYear()

    return [y, m+1, d].join("-")
  }

  // get full date time
  exports.get_date_time = () => {
    d = datetime.getDate()
    m = datetime.getMonth()
    y = datetime.getFullYear()

    h = datetime.getHours()
    mn = datetime.getMinutes()
    s = datetime.getSeconds()

    return [y, m, d].join("-")+" "+[h, mn, s].join(":")
  }

  // get YYYY-mm
  exports.get_periode = () => {
    var arr_d = this.date_full.split("-")
    // console.log(arr_d)
    return `${arr_d[0]}-${arr_d[1]}`
  }

  console.log(this.get_periode())

/**
description: function get string day dan month
parameter: nation is string and the values is "id" for indonesia or "en" for English 
call & return : get_string_day(nation) ==> return value for ex "Sunday" in "en" or "Minggu" in "id",
      get_string_month(nation) ==> return value for ex "January" in "en" or Januari in "id"  
*/

  // get single string day
  exports.get_string_day = (nation = "id") => {
    dy = datetime.getDay()
    
    return list_of_day[nation][parseInt(dy)]
  }

  // get single strign month
  exports.get_string_month = (nation = "id") => {
    g = datetime.getMonth()
    
    return list_of_month[nation][parseInt(g)]
  }

/**
description: function get single value of 
call = for ex get_single_date
*/

// get single date
exports.get_single_date = parseInt(datetime.getDate())

// get single month
exports.get_single_month = parseInt(datetime.getMonth())

// get single year
exports.get_single_year = parseInt(datetime.getFullYear())



// get single Hour
exports.get_single_hour = datetime.getHours()

// get single Minutes
exports.get_single_minutes = datetime.getMinutes()

// get single Second
exports.single_second =  datetime.getSeconds()


// get all date without separator
exports.no_sparate_date = this.year.toString()+this.month.toString()+this.date.toString()
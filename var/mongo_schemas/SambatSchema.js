/**
 * @author surya_hanggara
 * @email suryahanggara@gmail.com
 * @create date 2021-02-18 14:22:43
 * @modify date 2021-05-01 11:19:43
 * @desc [description]
 */

 exports.SAMBAT_SCHEMA = {
    sources : {type: String, required: true},
    methode : {type: String, required: true},
    status : {type: Boolean, required: true},
    status_msg : {type: String, required: true},
    time_ex : {type: String, required: true},
    periode : {type: String, required: true},
    tipe: {type: String, required: true},
    data: {type: Object, required: true}
}
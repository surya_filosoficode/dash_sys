/**
 * @author surya_hanggara, yoga_utama_putra
 * @email suryahanggara@gmail.com
 * @create date 2021-02-18 11:03:17
 * @modify date 2021-04-30 20:15:42
 * @desc [library for generate date values]
 */

const axios = require('axios')
const https = require('https')

const get_data = (url) => {
    return new Promise(resolve => {
        axios({
            method: 'get',
            url: url,
            timeout: 1000 * 30
        })
        .then(return_data => {
            resolve({
                "sts": true,
                "msg": "success",
                "data":return_data.data})
        })
        .catch(function (error) {
            // console.log(error)
            resolve({
                "sts": true,
                "msg": error,
                "data": ""})
            process.exit()
        })
    })
}

const get_data2 = (url, resolve) => {
    // return new Promise(resolve => {
        axios({
            method: 'get',
            url: url,
            timeout: 1000 * 30
        })
        .then(return_data => {
            resolve({
                "sts": true,
                "msg": "success",
                "data":return_data.data})
        })
        .catch(function (error) {
            // console.log(error)
            resolve({
                "sts": true,
                "msg": error,
                "data": ""})
            process.exit()
        })
    // })
}

const get_data_ssl = (url, resolve) => {
    // return new Promise(resolve => {
        axios({
            method: 'get',
            url: url,
            httpsAgent: new https.Agent({ rejectUnauthorized: false }),
            timeout: 1000 * 30
        })
        .then(return_data => {
            resolve({
                "sts": true,
                "msg": "success",
                "data":return_data.data})
        })
        .catch(function (error) {
            // console.log(error)
            resolve({
                "sts": true,
                "msg": error,
                "data": ""})
            process.exit()
        })
    // })
}

const get_data_basic_auth = (url, auth, resolve) => {
    // return new Promise(resolve => {
        axios({
            method: 'get',
            url: url,
            httpsAgent: new https.Agent({ rejectUnauthorized: false }),
            timeout: 1000 * 30,
            auth: {
                username: auth.username,
                password: auth.password
              }
        })
        .then(return_data => {
            resolve({
                "sts": true,
                "msg": "success",
                "data":return_data.data})
        })
        .catch(function (error) {
            // console.log(error)
            resolve({
                "sts": true,
                "msg": error,
                "data": ""})
            process.exit()
        })
}

const post_data_basic_auth = (url, auth, post_data, resolve) => {
    axios({
        method: 'post',
        url: url,
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
        timeout: 10000 * 30,
        data:post_data,
        auth: {
            username: auth.username,
            password: auth.password
            }
    })
    .then(return_data => {
        
        resolve({
            "sts": true,
            "msg": "success",
            "data":return_data.data})
    })
    .catch(function (error) {
        // console.log(error)
        resolve({
            "sts": true,
            "msg": error,
            "data": ""})
        process.exit()
    })
}


const post_data_basic = (url, post_data, resolve) => {
    axios({
        method: 'post',
        url: url,
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
        timeout: 1000 * 30,
        data:post_data
    })
    .then(return_data => {
        // console.log(return_data)
        resolve({
            "sts": true,
            "msg": "success",
            "data":return_data.data})
    })
    .catch(function (error) {
        // console.log(error)
        resolve({
            "sts": true,
            "msg": error,
            "data": ""})
        process.exit()
    })
}


module.exports = {get_data, get_data2, get_data_ssl, get_data_basic_auth, post_data_basic_auth, post_data_basic}
/**
 * @author surya_hanggara, yoga_utama_putra
 * @email suryahanggara@gmail.com
 * @create date 2021-02-18 11:03:17
 * @modify date 2021-05-05 12:22:04
 * @desc [library for generate date values]
 */

// kepegawaian
const kepegMg = require("../controller/kepegawaian/KepegawaianMg")
const kepegSql = require("../controller/kepegawaian/KepegawaianSql")

const runApps = () =>{
    kepegMg.set_data()
    kepegSql.set_data()
}

runApps()
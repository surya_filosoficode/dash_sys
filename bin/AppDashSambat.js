/**
 * @author surya_hanggara, yoga_utama_putra
 * @email suryahanggara@gmail.com
 * @create date 2021-02-18 11:03:17
 * @modify date 2021-05-05 12:20:01
 * @desc [library for generate date values]
 */


// sambat
const SambatFullMg = require("../controller/sambat/SambatFullMg")
const SambatFullSql = require("../controller/sambat/SambatFullSql")

const SambatKateMg = require("../controller/sambat/SambatKateMg")
const SambatKateSql = require("../controller/sambat/SambatKateSql")

const SambatSkpdMg = require("../controller/sambat/SambatSkpdMg")
const SambatSkpdSql = require("../controller/sambat/SambatSkpdSql")

const SambatSmsMg = require("../controller/sambat/SambatSmsMg")
const SambatSmsSql = require("../controller/sambat/SambatSmsSql")

const SambatWebMg = require("../controller/sambat/SambatWebMg")
const SambatWebSql = require("../controller/sambat/SambatWebSql")

const SambatTgSmsMg = require("../controller/sambat/SambatTgSmsMg")
const SambatTgSmsSql = require("../controller/sambat/SambatTgSmsSql")

const SambatTgWebMg = require("../controller/sambat/SambatTgWebMg")
const SambatTgWebSql = require("../controller/sambat/SambatTgWebSql")


const runApps = () =>{
    SambatFullMg.set_data()
    SambatFullSql.set_data()

    SambatKateMg.set_data()
    SambatKateSql.set_data()

    SambatSkpdMg.set_data()
    SambatSkpdSql.set_data()

    SambatSmsMg.set_data()
    SambatSmsSql.set_data()

    SambatWebMg.set_data()
    SambatWebSql.set_data()

    SambatTgSmsMg.set_data()
    SambatTgSmsSql.set_data()

    SambatTgWebMg.set_data()
    SambatTgWebSql.set_data()
}

runApps()
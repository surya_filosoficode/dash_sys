/**
 * @author surya_hanggara, yoga_utama_putra
 * @email suryahanggara@gmail.com
 * @create date 2021-02-18 11:03:17
 * @modify date 2021-04-30 20:15:42
 * @desc [library for generate date values]
 */

 const axios = require('axios')
 const https = require('https')
 
 const post_data = {jenis:'rekap'}
 const API_PDAM_ADUAN = "http://114.4.37.155:8081/info/getPengaduanPublish"

    axios({
        method: 'post',
        url: API_PDAM_ADUAN,
        httpsAgent: new https.Agent({ rejectUnauthorized: false }),
        timeout: 1000 * 30,
        data:post_data,
    })
    .then(return_data => {
        console.log(return_data.data)
    })
    .catch(function (error) {
        // console.log(error)
        console.log(error)
    })
 
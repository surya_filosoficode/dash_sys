/**
 * @author surya_hanggara, yoga_utama_putra
 * @email suryahanggara@gmail.com
 * @create date 2021-02-18 11:03:17
 * @modify date 2021-05-04 10:01:20
 * @desc [library for generate date values]
 */

// global lib


// local lib
const {get_data2, get_data_basic_auth} = require('../../lib/axios/MainAxios')
const date = require('../../lib/datetime/date')
const MysqlDbParam = require("../../var/app_dash/MysqlDbParam")
const {PKK_BASIC_AUTH} = require("../../var/app_dash/BasicAuthParam")
const ExtnPkk = require("./ExtnPkk")

// variable
const ListApiUrl = require('../../var/app_dash/ListApiUrl')

const TAG = "PKK_SQL_ANG_DW"
var date_in = date.date_full

const DB_NAME = MysqlDbParam.PKK_DB_NAME
var TABLE_NAME = MysqlDbParam.PKK_RW

console.time("run-apps")

// console.log(ListApiUrl.API_KEPEGAWAIAN)
exports.set_data = () => {
    var method = ": set_data: ";

    var data = get_data_basic_auth(ListApiUrl.URL_PARAM_PKK_RW, PKK_BASIC_AUTH, async (callback)=>{
        // console.log(callback)
        var main_data = []

        var arr_list = []
        
        if(callback.sts){
            main_data = callback.data

            // ----------------------logic data processing-------------------------
            var row = 1

            for (let i in main_data) {
                
                var tmp = [`${date_in}_${i}`,
                            `${main_data[i].kode_kelurahan}`,
                            `${(main_data[i].kelurahan).toUpperCase()}`,
                            `${main_data[i].kode_kecamatan}`,
                            `${(main_data[i].kecamatan).toUpperCase()}`,
                            `${main_data[i].jml_rw}`,
                            `${date_in}`,
                        ]
                
                arr_list.push(tmp)
                row++
            }

            // console.log(arr_list)
            // ----------------------logic data processing-------------------------

            await Promise.all(
                    [
                        ExtnPkk.insert_data_seris(DB_NAME, TABLE_NAME, arr_list)
                    ]
                ).then((values) => {
                    console.log(TAG+method+" method: end of run")
                    console.timeEnd("run-apps")
                });
        }

    })




}


// this.set_data()
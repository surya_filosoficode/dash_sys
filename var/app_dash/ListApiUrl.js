// api pajak
exports.API_PAJAK = "http://36.91.58.53:8088/kominfo/walikota.php"

// api kepegawaian
exports.API_KEPEGAWAIAN = "https://spm.malangkota.go.id/inject/inject_kepegawaian.php"

// api pdam
// const API_PDAM_MAIN = "http://localhost/inject_pdam/index.php?"

const API_PDAM_MAIN = "https://dashboard.malangkota.go.id/inject_pdam/index.php?"
    exports.API_PDAM_PELANGGAN_REKAP = API_PDAM_MAIN+"kategori=pelanggan-rekap"
    exports.API_PDAM_PELANGGAN_DETAIL = API_PDAM_MAIN+"kategori=pelanggan-detail"

    exports.API_PDAM_ADUAN_REKAP = API_PDAM_MAIN+"kategori=pengaduan-rekap"
    exports.API_PDAM_ADUAN_DETAIL = API_PDAM_MAIN+"kategori=pengaduan-detail"

    exports.API_PDAM_FTAB = API_PDAM_MAIN+"kategori=fountaintap"

// exports.API_PDAM_ADUAN_REKAP = "http://114.4.37.155:8081/info/getPengaduanPublish"
// exports.API_PDAM_ADUAN_DETAIL = "https://dashboard.malangkota.go.id/proxy/showpdam/get_pengaduan_detail"

// exports.API_PDAM_FTAB = "http://114.4.37.155:8081/info/getFountainTap"

// api sambat
exports.API_SAMBAT_MAIN = "https://spm.malangkota.go.id/inject/inject_sambat.php"
    exports.URL_PARAM_LINK_ADUAN = `https://spm.malangkota.go.id/inject_sambat/?kategori=aduan-masuk-date&date=`
    exports.URL_PARAM_WEB = `${this.API_SAMBAT_MAIN}?kategori=tiket-aduan-skpd-date&date=`
    exports.URL_PARAM_SMS = `${this.API_SAMBAT_MAIN}?kategori=sms-aduan-skpd-date&date=`
    exports.URL_PARAM_FULL = `${this.API_SAMBAT_MAIN}?kategori=tiket-aduan-full-join-date&date=`
    exports.URL_PARAM_KATEGORI = `${this.API_SAMBAT_MAIN}?kategori=tiket-aduan-jenis-kate-date&date=`
    exports.URL_PARAM_SKPD = `${this.API_SAMBAT_MAIN}?kategori=tiket-aduan-skpd-date&date=`
    exports.URL_PARAM_TSMS = `${this.API_SAMBAT_MAIN}?kategori=sms-tanggapan-skpd&date=`
    exports.URL_PARAM_TWEB = `${this.API_SAMBAT_MAIN}?kategori=tiket-tanggapan-skpd&date=`

// api pkk
exports.API_PKK = "https://simpkk.malangkota.go.id/inject_pkk/"
    exports.URL_PARAM_PKK_RT = `${this.API_PKK}?kategori=sum-rt-kelurahan`
    exports.URL_PARAM_PKK_RW = `${this.API_PKK}?kategori=sum-rw-kelurahan`
    exports.URL_PARAM_PKK_DW = `${this.API_PKK}?kategori=sum-dasawisma-kelurahan`
    exports.URL_PARAM_PKK_ANG_DW = `${this.API_PKK}?kategori=sum-ang-dasawisma-kelurahan`
    exports.URL_PARAM_PKK_PY = `${this.API_PKK}?kategori=sum-posyandu-kelurahan`
    exports.URL_PARAM_PKK_KP = `${this.API_PKK}?kategori=sum-kooperasi-kelurahan`

// api spm
exports.API_SPM = "https://spm.malangkota.go.id/dataapi/ws/"

    exports.URL_PARAM_SPM_P = `${this.API_SPM}kel_data_now`
    exports.URL_PARAM_SPM_PV = `${this.API_SPM}verif_data_now`
    exports.URL_PARAM_SPM_DN = `${this.API_SPM}dinsos_data_now`
    exports.URL_PARAM_SPM_PNY = `${this.API_SPM}penyakit_data_all`
    exports.URL_PARAM_SPM_RS = `${this.API_SPM}rs_data_all`

    // inject spm all
    exports.URL_INJECT_PARAM_SPM_P = `${this.API_SPM}kel_data_all`
    exports.URL_INJECT_PARAM_SPM_PV = `${this.API_SPM}verif_data_all`
    exports.URL_INJECT_PARAM_SPM_DN = `${this.API_SPM}dinsos_data_all`


const main_url_pendidikan = "https://spm.malangkota.go.id/inject/inject_pedidikan/index.php?kategori="

// const main_url_pendidikan = "http://103.135.14.154/api/"
    // exports.API_PD_MS_PAUD = main_url_pendidikan+"api_sp_01.php"
    // exports.API_PD_MS_DASAR = main_url_pendidikan+"api_sp_02.php"
    // exports.API_PD_MS_MENENGAH = main_url_pendidikan+"api_sp_03.php"
    // exports.API_PD_MS_NON_FORMAL = main_url_pendidikan+"api_sp_04.php"
    // exports.API_PD_MS_KHUSUS = main_url_pendidikan+"api_sp_05.php"

    // exports.API_PD_PENDIDIK = main_url_pendidikan+"api_ptk_01.php"

    // exports.API_PD_PESERTA_DIDIK = main_url_pendidikan+"api_pd_01.php"


    exports.API_PD_MS_PAUD = main_url_pendidikan+"API_PD_MS_PAUD"
    exports.API_PD_MS_DASAR = main_url_pendidikan+"API_PD_MS_DASAR"
    exports.API_PD_MS_MENENGAH = main_url_pendidikan+"API_PD_MS_MENENGAH"
    exports.API_PD_MS_NON_FORMAL = main_url_pendidikan+"API_PD_MS_NON_FORMAL"
    exports.API_PD_MS_KHUSUS = main_url_pendidikan+"API_PD_MS_KHUSUS"
    exports.API_PD_PENDIDIK = main_url_pendidikan+"API_PD_PENDIDIK"
    exports.API_PD_PESERTA_DIDIK = main_url_pendidikan+"API_PD_PESERTA_DIDIK"

/**
 * @author surya_hanggara, yoga_utama_putra
 * @email suryahanggara@gmail.com
 * @create date 2021-02-18 11:03:17
 * @modify date 2021-05-05 12:22:15
 * @desc [library for generate date values]
 */

// pajak
const  PdPendidikMg = require("../controller/pendidikan/PdPendidikMg")
const  PdPendidikSql = require("../controller/pendidikan/PdPendidikSql")

const  PdPesertaDidikMg = require("../controller/pendidikan/PdPesertaDidikMg")
const  PdPesertaDidikSql = require("../controller/pendidikan/PdPesertaDidikSql")

const  PdSklhKsMg = require("../controller/pendidikan/PdSklhKsMg")
const  PdSklhKsSql = require("../controller/pendidikan/PdSklhKsSql")

const  PdSklhNonFormalMg = require("../controller/pendidikan/PdSklhNonFormalMg")
const  PdSklhNonFormalSql = require("../controller/pendidikan/PdSklhNonFormalSql")

const  PdSklhPaudMg = require("../controller/pendidikan/PdSklhPaudMg")
const  PdSklhPaudSql = require("../controller/pendidikan/PdSklhPaudSql")

const  PdSklhSdMg = require("../controller/pendidikan/PdSklhSdMg")
const  PdSklhSdSql = require("../controller/pendidikan/PdSklhSdSql")


const runApps = () =>{
    PdPendidikMg.set_data()
    PdPendidikSql.set_data()

    PdPesertaDidikMg.set_data()
    PdPesertaDidikSql.set_data()

    PdSklhKsMg.set_data()
    PdSklhKsSql.set_data()

    PdSklhNonFormalMg.set_data()
    PdSklhNonFormalSql.set_data()

    PdSklhPaudMg.set_data()
    PdSklhPaudSql.set_data()

    PdSklhSdMg.set_data()
    PdSklhSdSql.set_data()

}

runApps()
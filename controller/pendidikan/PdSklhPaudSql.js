/**
 * @author surya_hanggara, yoga_utama_putra
 * @email suryahanggara@gmail.com
 * @create date 2021-02-18 11:03:17
 * @modify date 2021-06-11 14:38:58
 * @desc [library for generate date values]
 */

// global lib


// local lib
const {post_data_basic_auth, get_data_basic_auth} = require('../../lib/axios/MainAxios')
const date = require('../../lib/datetime/date')
const MysqlDbParam = require("../../var/app_dash/MysqlDbParam")
const {PDD_BASIC_AUTH} = require("../../var/app_dash/BasicAuthParam")
const ExtnPdam = require("./ExtnPendidikan")

// variable
const ListApiUrl = require('../../var/app_dash/ListApiUrl')

const TAG = "PENDIDIKAN_PAUD_SQL"
var date_in = date.date_full

const DB_NAME = MysqlDbParam.PENDIDIKAN_DB_NAME
var TABLE_NAME = MysqlDbParam.PD_MASTER_SKLH

const tipe = "paud"
console.time("run-apps")

// console.log(ListApiUrl.API_KEPEGAWAIAN)
exports.set_data = () => {
    var method = ": set_data: ";
    const post_data = {token:'67A84C2614290BAEEBC797C2822EF6480E38EEC0137812C7A890C9B97321DFF9EEE49F1A56D8FB3DDF6F58F28F530BA8D92E434D1C7B9242FB75FF29E7D18318'}
    
    var data = post_data_basic_auth(ListApiUrl.API_PD_MS_PAUD, PDD_BASIC_AUTH, post_data, async (callback)=>{
        // console.log(callback)
        // process.exit()

        const str_dates = date.get_date()

        const sts_req = callback.sts
        const msg_req = callback.msg  
        
        var arr_list = []

        if(sts_req){
            const data_req = callback.data
            const title = data_req.title
            const item = data_req.item
            
            // ----------------------logic data processing-------------------------
            var row = 1

            for (let i in item) {

                // console.log(item[i])
                // process.exit()
                if(item[i]){
                    // console.log("ok")
                    var kode_wilayah = item[i].kode_wilayah
                    var nama = item[i].nama
                    var tkn = item[i].tkn
                    var tks = item[i].tks
                    var kbn = item[i].kbn
                    var kbs = item[i].kbs
                    var tpan = item[i].tpan
                    var tpas = item[i].tpas
                    var spsn = item[i].spsn
                    var spss = item[i].spss
                    var ran = item[i].ran
                    var ras = item[i].ras
                   

                    
                    arr_list.push(["", date_in, parseInt(kode_wilayah).toString(), nama, "SD NEGERI", tipe, tkn.toString()])
                    arr_list.push(["", date_in, parseInt(kode_wilayah).toString(), nama, "SD SWASTA", tipe, tks.toString()])
                    arr_list.push(["", date_in, parseInt(kode_wilayah).toString(), nama, "SMP NEGERI", tipe, kbn.toString()])
                    arr_list.push(["", date_in, parseInt(kode_wilayah).toString(), nama, "SMP SWASTA", tipe, kbs.toString()])
                    arr_list.push(["", date_in, parseInt(kode_wilayah).toString(), nama, "SMA NEGERI", tipe, tpan.toString()])
                    arr_list.push(["", date_in, parseInt(kode_wilayah).toString(), nama, "SMA SWASTA", tipe, tpas.toString()])
                    arr_list.push(["", date_in, parseInt(kode_wilayah).toString(), nama, "SMK NEGERI", tipe, spsn.toString()])
                    arr_list.push(["", date_in, parseInt(kode_wilayah).toString(), nama, "SMK SWASTA", tipe, spss.toString()])
                    arr_list.push(["", date_in, parseInt(kode_wilayah).toString(), nama, "MIN NEGERI", tipe, ran.toString()])
                    arr_list.push(["", date_in, parseInt(kode_wilayah).toString(), nama, "MIN SWASTA", tipe, ras.toString()])
                    // console.log(tmp)
                }
            }

            // console.log(arr_list)
            // process.exit()
            // ----------------------logic data processing-------------------------

            await Promise.all(
                    [
                        ExtnPdam.insert_data_seris_tipe(DB_NAME, TABLE_NAME, tipe, arr_list)
                    ]
                ).then((values) => {
                    console.log(TAG+method+" method: end of run")
                    console.timeEnd("run-apps")
                });
        }

    })




}


// this.set_data()
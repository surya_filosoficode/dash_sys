/**
 * @author surya_hanggara, yoga_utama_putra
 * @email suryahanggara@gmail.com
 * @create date 2021-02-18 11:03:17
 * @modify date 2021-05-07 15:15:35
 * @desc [library for generate date values]
 */

// global lib


// local lib
const mongoModel = require('../../lib/mongo/conn_mongo')
const {get_data2} = require('../../lib/axios/MainAxios')
const date = require('../../lib/datetime/date')

// variable
const ListApiUrl = require('../../var/app_dash/ListApiUrl')
const MongoPath = require('../../var/app_dash/MongoPath')
const mongoSchema = require("../../var/mongo_schemas/Default")

// const extnData = require('./ExtnPajakMg')

const TAG = "PAJAK_MONGO"

const mongoBase = MongoPath.PATH_PAJAK

console.time("run-apps")

exports.set_data = () => {
    var method = ": mongo: set_data_mongo: ";

    var data = get_data2(ListApiUrl.API_PAJAK, callback => {
        var data =  callback
        // console.log(callback);
        var main_data = []
    
        var sum_target = 0
        var sum_realisasi = 0

        // console.log(data)
        var sts = false
        var msg = "error"

        // console.log("ok")
            main_data = data.data

            var row = 1
            for (let i in main_data) {
                var target = parseInt(main_data[i].Target)
                var realisasi = parseInt(main_data[i].Realisasi)
            
                sum_target += target
                sum_realisasi += realisasi

                row++
            }

            sts = true
            msg = "success"

            var data_sum = {"sum_target": sum_target, "sum_realisasi": sum_realisasi}


            var where = {periode:date.get_date()}

            var arr_data = {
                "sources":ListApiUrl.API_PAJAK,
                "methode":"get",
                "status":true,
                "status_msg":"success",
                "time_ex": date.get_date_time(),
                "periode": date.get_date(),
                "data":main_data
            }

            var arr_data_sums = {
                "sources":ListApiUrl.API_PAJAK,
                "methode":"get",
                "status":true,
                "status_msg":"success",
                "time_ex": date.get_date_time(),
                "periode": date.get_date(),
                "data":data_sum
            }

            // extnData.insert_seris(mongoBase, MongoPath.CL_PAJAK_ALL, mongoSchema.DEFAULT_SCHEMA, where, arr_data)
            
            var mongoModels = mongoModel.mongoModel(mongoBase, MongoPath.CL_PAJAK_ALL, mongoSchema.DEFAULT_SCHEMA)
            mongoModels.find(where, function (err, docs) {
                // console.log(docs)
                // if(err)console.log(err)
                if(docs.length == 0){
                    // console.log("insert")
                    var method = "insert_seris: "

                    var msg = new mongoModels(arr_data)
                    msg.save().
                        then(doc => {
                            // console.log(doc)
                            console.log(TAG+method+" true")
                        }).catch(err => {
                            console.log(TAG+method+" false: "+err)
                        })
                }else{
                    // console.log("update")
                    mongoModels.findOneAndUpdate(where, arr_data, function(err, docs){
                        var method = "update_seris: "
                        var return_tmp = {"sts": false, "msg": err, "data":{}}
                        if(!err){
                            console.log(TAG+method+" true")
                            return_tmp = {"sts": true, "msg": "success", "data":docs}
                        }else{
                            console.log(TAG+method+" false: "+err)
                        }
                    })
                }
            })

            var modelSum = mongoModel.mongoModel(mongoBase, MongoPath.CL_PAJAK_SUM, mongoSchema.DEFAULT_SCHEMA)
            modelSum.find(where, function (err, docs) {
                // console.log(docs)
                if(err)console.log(err)
                if(docs.length == 0){
                    var method = "insert_sums: "
                    // console.log("insert")
                    var msg = new modelSum(arr_data_sums)
                    // console.log(docs)
                    msg.save().
                        then(doc => {
                            console.log(TAG+method+" true")
                            console.log(doc)
                        }).catch(err => {
                            console.log(TAG+method+" false: "+err)
                            console.log(err)
                        })
                }else{
                    var method = "update_sums: "
                    // console.log("update")
                    modelSum.findOneAndUpdate(where, arr_data_sums, function(err, docs){
                        var return_tmp = {"sts": false, "msg": err, "data":{}}
                        if(!err){
                            console.log(TAG+method+" true")
                            return_tmp = {"sts": true, "msg": "success", "data":docs}
                        }else{
                            console.log(TAG+method+" false: "+err)
                        }
                    })
                }
            })

    })
}


// module.exports = {}
// this.set_data()
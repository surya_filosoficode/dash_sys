/**
 * @author surya_hanggara
 * @email suryahanggara@gmail.com
 * @create date 2021-02-18 14:51:03
 * @modify date 2021-05-07 15:26:31
 * @desc [description]
 */

const mongoose = require('mongoose')

mongoSchema = (obj_schema) => {
    return mongoose.Schema(obj_schema)
}

mongoConnection = (mongoPath) => {
    return con_mongo = mongoose.createConnection(mongoPath, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
}


// mongoose.set('useFindAndModify', false)
exports.mongoModel = (mongoModel, collection, obj_schema) => {
    mongoose.set('useFindAndModify', false)
    return mongoConnection(mongoModel).model(collection, mongoSchema(obj_schema))
}
/**
 * @author surya_hanggara, yoga_utama_putra
 * @email suryahanggara@gmail.com
 * @create date 2021-02-18 11:03:17
 * @modify date 2021-06-11 14:40:11
 * @desc [library for generate date values]
 */

// pajak
const PdamPlgMg = require("../controller/pdam/PdamPlgMg")
const PdamPlgSql = require("../controller/pdam/PdamPlgSql")

const PdamAduanMg = require("../controller/pdam/PdamAduanMg")
const PdamAduanSql = require("../controller/pdam/PdamAduanSql")

const PdamFtabSerisMg = require("../controller/pdam/PdamFtabSerisMg")
const PdamFtabSerisSql = require("../controller/pdam/PdamFtabSerisSql")

const PdamFtabMainMg = require("../controller/pdam/PdamFtabMainMg")

const runApps = () =>{
    PdamPlgMg.set_data()
    PdamPlgSql.set_data()
    
    PdamAduanMg.set_data()
    PdamAduanSql.set_data()

    PdamFtabSerisMg.set_data()
    PdamFtabSerisSql.set_data()

    PdamFtabMainMg.set_data()
}

runApps()
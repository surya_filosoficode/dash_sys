/**
 * @author surya_hanggara
 * @email suryahanggara@gmail.com
 * @create date 2021-02-18 11:03:17
 * @modify date 2021-05-03 06:41:43
 * @desc [library for generate date values]
 */

// global lib


// local lib
const MysqlDbParam = require("../app_dash/MysqlDbParam")
const { connect, connect2 } = require('../../lib/mysql/bin/connection')
const date = require('../../lib/datetime/date')


// variable
const TAG = "Pajak Mpdel"

var date_in = date.date_full

const DB_NAME = MysqlDbParam.PAJAK_DB_NAME

const CUR_ALL = MysqlDbParam.PAJAK_CUR_ALL
const CUR_SUM = MysqlDbParam.PAJAK_CUR_SUM
const SERIS_ALL = MysqlDbParam.PAJAK_SERIS_ALL
const SERIS_SUM = MysqlDbParam.PAJAK_SERIS_SUM


exports.check_single_sum = (resolve) => {
    var method = ": check_single_sum: "
    console.log(TAG+method+"cek data")

    var sql_statement = "SELECT * FROM "+CUR_SUM+" WHERE id= ? "
    var where = [0]
    
    connect2(DB_NAME, sql_statement, where, (callback) =>{
            resolve(callback)
        })
        
}

exports.check_single_all = (resolve) => {
    var method = ": check_single_all: "
    console.log(TAG+method+"cek data")

    var sql_statement = "SELECT * FROM "+CUR_ALL+" WHERE 1 "
    var where = [0]

    
    connect2(DB_NAME, sql_statement, where, (callback) =>{
            resolve(callback)
        })
}

exports.save_single_sql_sum = (data, resolve) => {
    var method = ": save_single_sql_sum: "
    console.log(TAG+method+"insert")

    var sql_statement = "INSERT INTO "+CUR_SUM+" (id, target, realisasi) VALUES ( ? ) "
    var where = [data]
    
    // return connect(DB_NAME, schemas.insert_cur_sum().sql_statement, [data])

    connect2(DB_NAME, sql_statement, where, (callback) =>{
            resolve(callback)
        })
}

exports.save_single_sql_all = (data, resolve) => {
    var method = ": save_single_sql_all: "
    console.log(TAG+method+"insert")

    var sql_statement = "INSERT INTO "+CUR_ALL+" VALUES ? "
    var where = [data]
    
    // return connect(DB_NAME, schemas.insert_cur_all().sql_statement, [data])

    connect2(DB_NAME, sql_statement, where, (callback) =>{
            resolve(callback)
        })
} 

exports.delete_single_sum = (resolve) => {
    var method = ": delete_single_sum: "
    console.log(TAG+method+"delete")

    var sql_statement = "DELETE FROM "+CUR_SUM+" WHERE 1"
    var where = [0]
    // console.log(schemas.delete_cur_sum().sql_statement)
    
    // return connect(DB_NAME, schemas.delete_cur_sum().sql_statement, [0])
    connect2(DB_NAME, sql_statement, where, (callback) =>{
            resolve(callback)
        })
}

exports.delete_single_all = (resolve) => {
    var method = ": delete_single_all: "
    console.log(TAG+method+"delete")

    var sql_statement = "DELETE FROM "+CUR_ALL+" WHERE 1"
    var where = [0]
    // console.log(schemas.delete_cur_sum().sql_statement)
    
    // return connect(DB_NAME, schemas.delete_cur_sum().sql_statement, [0])
    connect2(DB_NAME, sql_statement, where, (callback) =>{
            resolve(callback)
        })
}


exports.check_seris_sum = (resolve) => {
    var method = ": check_seris_sum: "
    console.log(TAG+method+"cek data")

    var sql_statement = "SELECT * FROM "+SERIS_SUM+" WHERE periode = ? "
    var where = [date_in]
    // console.log(schemas.select_cur_sum().sql_statement)
    
    // return connect(DB_NAME, schemas.select_cur_sum().sql_statement, [0])
    
    connect2(DB_NAME, sql_statement, where, (callback) =>{
            resolve(callback)
        })
        
}

exports.check_seris_all = (resolve) => {
    var method = ": check_seris_all: "
    console.log(TAG+method+"cek data")

    var sql_statement = "SELECT * FROM "+SERIS_ALL+" WHERE periode = ? "
    var where = [date_in]
    // console.log(schemas.select_cur_all().sql_statement)
    
    // return connect(DB_NAME, schemas.select_cur_all().sql_statement, [0])

    
    connect2(DB_NAME, sql_statement, where, (callback) =>{
            resolve(callback)
        })
}

exports.save_seris_sql_sum = (data, resolve) => {
    var method = ": save_pajak_sql_sum: "
    console.log(TAG+method+"insert")
    
    var sql_statement = "INSERT INTO "+SERIS_SUM+" (id, periode, target, realisasi) VALUES ( ? ) "
    var where = [data]
    // return connect(DB_NAME, schemas.insert_cur_sum().sql_statement, [data])

    connect2(DB_NAME, sql_statement, where, (callback) =>{
            resolve(callback)
        })
}

exports.save_seris_sql_all = (data, resolve) => {
    var method = ": save_pajak_sql_all: "
    console.log(TAG+method+"insert")

    var sql_statement = "INSERT INTO "+SERIS_ALL+" (id, periode, jenis, target, realisasi) VALUES ? "
    var where = [data]
    
    // return connect(DB_NAME, schemas.insert_cur_all().sql_statement, [data])

    connect2(DB_NAME, sql_statement, where, (callback) =>{
            resolve(callback)
        })
} 

exports.delete_seris_sum = (resolve) => {
    var method = ": delete_seris_sum: "
    console.log(TAG+method+"delete")

    var sql_statement = "DELETE FROM "+SERIS_SUM+" WHERE  periode = ?"
    var where = [date_in]
    // console.log(schemas.delete_cur_sum().sql_statement)
    
    // return connect(DB_NAME, schemas.delete_cur_sum().sql_statement, [0])
    connect2(DB_NAME, sql_statement, where, (callback) =>{
            resolve(callback)
        })
}

exports.delete_seris_all = (resolve) => {
    var method = ": delete_seris_all: "
    console.log(TAG+method+"delete")

    var sql_statement = "DELETE FROM "+SERIS_ALL+" WHERE  periode = ?"
    var where = [date_in]
    // console.log(schemas.delete_cur_sum().sql_statement)
    
    // return connect(DB_NAME, schemas.delete_cur_sum().sql_statement, [0])
    connect2(DB_NAME, sql_statement, where, (callback) =>{
            resolve(callback)
        })
}
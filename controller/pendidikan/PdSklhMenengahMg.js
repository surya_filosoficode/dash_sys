/**
 * @author surya_hanggara, yoga_utama_putra
 * @email suryahanggara@gmail.com
 * @create date 2021-02-18 11:03:17
 * @modify date 2021-06-11 14:38:59
 * @desc [library for generate date values]
 */

// global lib


// local lib
const {post_data_basic_auth} = require('../../lib/axios/MainAxios')
const date = require('../../lib/datetime/date')
const mongoModel = require("../../lib/mongo/conn_mongo")

// variable
const ListApiUrl = require('../../var/app_dash/ListApiUrl')
const MongoPath = require('../../var/app_dash/MongoPath')
const mongoSchema = require("../../var/mongo_schemas/Pendidikan")
const {PDD_BASIC_AUTH} = require("../../var/app_dash/BasicAuthParam")

const TAG = "PENDIDIKAN_SEKOLAH_MENENGAH"

const mongoBase = MongoPath.PATH_PENDIDIKAN

console.time("run-apps")


exports.set_data = () => {
    var method = ": mongo: set_data: ";
    const post_data = {token:'67A84C2614290BAEEBC797C2822EF6480E38EEC0137812C7A890C9B97321DFF9EEE49F1A56D8FB3DDF6F58F28F530BA8D92E434D1C7B9242FB75FF29E7D18318'}
    var data = post_data_basic_auth(ListApiUrl.API_PD_MS_MENENGAH, PDD_BASIC_AUTH, post_data, callback =>{
        var main_data = []

        // console.log(callback)
        // process.exit()

        const str_dates = date.get_periode()

        const sts_req = callback.sts
        const msg_req = callback.msg
        
        const data_req = callback.data
            const title = data_req.title
            const item = data_req.item
        
        var where = {periode:str_dates, type:"menengah"}

        var arr_data = {
            "sources":ListApiUrl.API_PD_MS_MENENGAH,
            "methode":"get",
            "status":true,
            "status_msg":"success",
            "time_ex": date.get_date_time(),
            "periode": str_dates,
            "type": "menengah",
            "data":item
        }
        // console.log(arr_data);

        var mongoModels = mongoModel.mongoModel(mongoBase, MongoPath.CL_MASTER_SKLH, mongoSchema.PD_SCHEMA)
        mongoModels.find(where, function (err, docs) {
            if(docs.length == 0){
                var method = ": insert_seris: "

                var msg = new mongoModels(arr_data)
                msg.save().
                    then(doc => {
                        console.log(TAG+method+" true")
                    }).catch(err => {
                        console.log(TAG+method+" false: "+err)
                    })
            }else{
                mongoModels.findOneAndUpdate(where, arr_data, function(err, docs){
                    var method = ": update_seris: "
                    var return_tmp = {"sts": false, "msg": err, "data":{}}
                    if(!err){
                        console.log(TAG+method+" true")
                        return_tmp = {"sts": true, "msg": "success", "data":docs}
                    }else{
                        console.log(TAG+method+" false: "+err)
                    }
                })
            }
        })

    })
}




// this.set_data()

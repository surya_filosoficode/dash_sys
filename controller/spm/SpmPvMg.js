/**
 * @author surya_hanggara, yoga_utama_putra
 * @email suryahanggara@gmail.com
 * @create date 2021-02-18 11:03:17
 * @modify date 2021-05-04 15:15:01
 * @desc [library for generate date values]
 */

// global lib


// local lib
const {get_data_basic_auth} = require('../../lib/axios/MainAxios')
const date = require('../../lib/datetime/date')
const mongoModel = require("../../lib/mongo/conn_mongo")

// variable
const ListApiUrl = require('../../var/app_dash/ListApiUrl')
const MongoPath = require('../../var/app_dash/MongoPath')
const mongoSchema = require("../../var/mongo_schemas/Default")
const {SPM_BASIC_AUTH} = require("../../var/app_dash/BasicAuthParam")

const TAG = "SPM_PV_MONGO"

const mongoBase = MongoPath.PATH_SPM

console.time("run-apps")

exports.set_data = () => {
    var method = ": mongo: set_data: ";

    // console.log()

    var data = get_data_basic_auth(ListApiUrl.URL_PARAM_SPM_PV, SPM_BASIC_AUTH, async callback =>{
        var main_data = {
            "th": date.get_single_year,
            "periode": date.month,
            "val": 0
        }
        
        if(callback.data.lengt > 0){
            main_data = callback
        }
        // console.log(main_data)
        

        var where = {periode:date.get_date()}

        var where_main = {methode: "get"}

        var arr_data = {
            "sources":ListApiUrl.URL_PARAM_SPM_PV,
            "methode":"get",
            "status":true,
            "status_msg":"success",
            "time_ex": date.get_date_time(),
            "periode": date.get_date(),
            "data":main_data
        }


        var mongoModels = mongoModel.mongoModel(mongoBase, MongoPath.CL_SPM_SERIS_PV, mongoSchema.DEFAULT_SCHEMA)
        mongoModels.find(where, function (err, docs) {
            // console.log(docs)
            // if(err)console.log(err)
            if(docs.length == 0){
                // console.log("insert")
                var method = ": insert_seris: "

                var msg = new mongoModels(arr_data)
                msg.save().
                    then(doc => {
                        // console.log(doc)
                        console.log(TAG+method+" true")
                    }).catch(err => {
                        console.log(TAG+method+" false: "+err)
                    })
            }else{
                // console.log("update")
                mongoModels.findOneAndUpdate(where, arr_data, function(err, docs){
                    var method = ": update_seris: "
                    var return_tmp = {"sts": false, "msg": err, "data":{}}
                    if(!err){
                        console.log(TAG+method+" true")
                        return_tmp = {"sts": true, "msg": "success", "data":docs}
                    }else{
                        console.log(TAG+method+" false: "+err)
                    }
                })
            }
        })

        var mongoSums = mongoModel.mongoModel(mongoBase, MongoPath.CL_SPM_CUR_PV, mongoSchema.DEFAULT_SCHEMA)
        mongoSums.find(where_main, function (err, docs) {
            // console.log(docs)
            // if(err)console.log(err)
            if(docs.length == 0){
                // console.log("insert")
                var method = ": insert_mains: "

                var msg = new mongoSums(arr_data)
                msg.save().
                    then(doc => {
                        // console.log(doc)
                        console.log(TAG+method+" true")
                    }).catch(err => {
                        console.log(TAG+method+" false: "+err)
                    })
            }else{
                // console.log("update")
                mongoSums.findOneAndUpdate(where_main, arr_data, function(err, docs){
                    var method = ": update_mains: "
                    var return_tmp = {"sts": false, "msg": err, "data":{}}
                    if(!err){
                        console.log(TAG+method+" true")
                        return_tmp = {"sts": true, "msg": "success", "data":docs}
                    }else{
                        console.log(TAG+method+" false: "+err)
                    }
                })
            }
        })
    })
}


// this.set_data()

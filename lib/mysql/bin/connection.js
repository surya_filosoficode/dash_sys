/**
 * @author surya_hanggara, yoga_utama_putra
 * @email suryahanggara@gmail.com
 * @create date 2021-02-18 11:03:17
 * @modify date 2021-05-04 12:17:37
 * @desc [library for generate date values]
 */

const mysql = require('mysql');
// const { promises } = require('node:fs');
const {basicConf, normalConf} = require('../config/db_conf')

function connect (db_name, str_sql, where){
  var db = mysql.createConnection(normalConf(db_name)); //nama database
  return new Promise(resolve =>{
    const re_data = {status:false, message:"", data:[]}
    db.query(str_sql, where, (err, result) => {
        if (err) {
              re_data.message = err
              // callback(re_data)
        } else {
              re_data.status = true
              re_data.message = "success"
              re_data.data = result

              // callback(re_data)
        }
        resolve(re_data)
    })
  })
}

function connect2 (db_name, str_sql, where, callback){
  var db = mysql.createConnection(normalConf(db_name)); //nama database
  // return new Promise(resolve =>{
    const re_data = {status:false, message:"", data:[]}
    db.query(str_sql, where, (err, result) => {
        if (err) {
              re_data.message = err
              // callback(re_data)
        } else {
              re_data.status = true
              re_data.message = "success"
              re_data.data = result

              // callback(re_data)
              // console.log(result)
        }

        // console.log(re_data)
        callback(re_data)
        // resolve(re_data)
    })
  // })
}


module.exports = {connect, connect2}
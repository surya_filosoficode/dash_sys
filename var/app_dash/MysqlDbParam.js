/**
 * @author surya_hanggara, yoga_utama_putra
 * @email suryahanggara@gmail.com
 * @create date 2021-02-18 11:03:17
 * @modify date 2021-06-11 13:57:50
 * @desc [library for generate date values]
 */

exports.PAJAK_DB_NAME = "dash_pajak"
    exports.PAJAK_CUR_ALL = "pajak_d_cur"
    exports.PAJAK_CUR_SUM = "pajak_cur"
    exports.PAJAK_SERIS_ALL = "pajak_d_seris"
    exports.PAJAK_SERIS_SUM = "pajak_seris"


exports.KAPEG_DB_NAME = "dash_kepegawaian"
    exports.KAPEG_CUR_ALL = "seris_kepeg_main"
    exports.KAPEG_CUR_SUM = "cur_kepeg_main"

    exports.KAPEG_CUR_STATUS = "cur_kepeg_status"
    exports.KAPEG_CUR_JK = "cur_kepeg_jk"
    exports.KAPEG_CUR_GOL_DETAIL = "cur_kepeg_gol_detail"
    exports.KAPEG_CUR_GOL = "cur_kepeg_gol"
    exports.KAPEG_CUR_AGAMA = "cur_kepeg_agama"


exports.PDAM_DB_NAME = "dash_pdam"
    exports.PDAM_SERIS_PELANGGAN = "seris_pelanggan"
    exports.PDAM_SERIS_ADUAN = "seris_aduan"

    exports.PDAM_MAIN_FTAB = "main_fountentab"
    exports.PDAM_SERIS_FTAB = "seris_fountentab"


exports.SAMBAT_DB_NAME = "dash_sambat"
    exports.SAMBAT_FULL = "sm_full"

    exports.SAMBAT_SKPD = "sm_skpd"
    exports.SAMBAT_JENIS = "sm_jn_kate"
    exports.SAMBAT_TANGGAPAN = "sm_tanggapan"
    exports.SAMBAT_ADUAN_MASUK = "sm_aduan_masuk"
    exports.SAMBAT_ACC_MASUK = "sm_acc_masuk"


exports.SPM_DB_NAME = "dash_spm"
    
    exports.SPM_CUR_DN= "cur_dn_main"
    exports.SPM_SERIS_DN = "dn_seris"

    exports.SPM_CUR_P = "cur_p_main"
    exports.SPM_SERIS_P = "p_seris"

    exports.SPM_CUR_PV = "cur_pv_main"
    exports.SPM_SERIS_PV = "pv_seris"

    exports.SPM_CUR_PENYAKIT = "pny_main_all"
    exports.SPM_CUR_RS = "rs_main_all"


exports.PKK_DB_NAME = "dash_pkk"
    
    exports.PKK_RT= "sum_rt_kel"
    exports.PKK_RW = "sum_rw_kel"

    exports.PKK_PY = "sum_py_kel"
    exports.PKK_KP = "sum_kp_kel"

    exports.PKK_DW = "sum_dw_kel"
    exports.PKK_ANGGOTA_DW = "sum_ang_dw_kel"


exports.PENDIDIKAN_DB_NAME = "dash_pendidikan"

    exports.PD_PENDIDIK= "data_pendidik"
    exports.PD_PESERTA_DIDIK = "data_peserta"
    exports.PD_MASTER_SKLH = "data_sekolah"
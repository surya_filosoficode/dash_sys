/**
 * @author surya_hanggara, yoga_utama_putra
 * @email suryahanggara@gmail.com
 * @create date 2021-02-18 11:03:17
 * @modify date 2021-04-30 09:34:39
 * @desc [library for generate date values]
 */

const mysql = require('mysql');

let basicConf = (req,res) => {
  return {connectionLimit: 10,
  host: "localhost",    // host
  user: "root",         // user
  password: "", // pass
  database: req}
}

let normalConf = (param) => {
  return {
  host: 'localhost',
  user: 'root',
  password: '',
  database: param}
}


module.exports = {basicConf, normalConf}
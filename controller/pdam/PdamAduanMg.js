/**
 * @author surya_hanggara, yoga_utama_putra
 * @email suryahanggara@gmail.com
 * @create date 2021-02-18 11:03:17
 * @modify date 2021-06-11 14:38:59
 * @desc [library for generate date values]
 */

// global lib


// local lib
const {get_data_basic_auth, post_data_basic_auth} = require('../../lib/axios/MainAxios')
const date = require('../../lib/datetime/date')
const mongoModel = require("../../lib/mongo/conn_mongo")

// variable
const ListApiUrl = require('../../var/app_dash/ListApiUrl')
const MongoPath = require('../../var/app_dash/MongoPath')
const mongoSchema = require("../../var/mongo_schemas/Default")
const {PDAM_BASIC_AUTH} = require("../../var/app_dash/BasicAuthParam")

const TAG = "PDAM_ADUAN_REKAP"

const mongoBase = MongoPath.PATH_PDAM

console.time("run-apps")


exports.set_data = () => {
    var method = ": mongo: set_data: ";
    const post_data = {jenis:'rekap'}
    var data = post_data_basic_auth(ListApiUrl.API_PDAM_ADUAN_REKAP, PDAM_BASIC_AUTH, post_data, callback =>{
        var main_data = []

        var th = ""
        var m = ""
        var d = ""

        var d_data = callback.data.data
        // console.log(callback)
        // process.exit()

        var str_dates = "0000-00-00"
        if(typeof callback == "object"){
            if(callback.sts){
                // console.log("ok")
                main_data = d_data.pengaduan
                    var data_periode = d_data.periode
                    // console.log(main_data)
                    th = data_periode.substr(0, 4)
                    m = data_periode.substr(4, 2)
                    d = date.get_single_date

                    str_dates = th+"-"+m+"-"+d
            }
        }
        
        var where = {periode:str_dates}

        var arr_data = {
            "sources":ListApiUrl.API_PDAM_ADUAN_DETAIL,
            "methode":"get",
            "status":true,
            "status_msg":"success",
            "time_ex": date.get_date_time(),
            "periode": str_dates,
            "data":callback.data.data.pengaduan
        }
        // console.log(arr_data);

        var mongoModels = mongoModel.mongoModel(mongoBase, MongoPath.CL_PDAM_ADUAN_SERIS, mongoSchema.DEFAULT_SCHEMA)
        mongoModels.find(where, function (err, docs) {
            if(docs.length == 0){
                var method = ": insert_seris: "

                var msg = new mongoModels(arr_data)
                msg.save().
                    then(doc => {
                        console.log(TAG+method+" true")
                    }).catch(err => {
                        console.log(TAG+method+" false: "+err)
                    })
            }else{
                mongoModels.findOneAndUpdate(where, arr_data, function(err, docs){
                    var method = ": update_seris: "
                    var return_tmp = {"sts": false, "msg": err, "data":{}}
                    if(!err){
                        console.log(TAG+method+" true")
                        return_tmp = {"sts": true, "msg": "success", "data":docs}
                    }else{
                        console.log(TAG+method+" false: "+err)
                    }
                })
            }
        })

    })
}




// this.set_data()

/**
 * @author surya_hanggara, yoga_utama_putra
 * @email suryahanggara@gmail.com
 * @create date 2021-02-18 11:03:17
 * @modify date 2021-06-11 13:45:21
 * @desc [library for generate date values]
 */

// global lib


// local lib
const model = require("../../var/model_sql/PendidikanModel")

const TAG = "PENDIDIKAN_MODEL"


exports.insert_data = (DB_NAME, tbl_name, data) => {
    return new Promise(cb => {
        model.check_cur(DB_NAME, tbl_name, resolve => {
            var method = ": sql: ins_data: ";
            // console.log(resolve)
            if(resolve.data.length == 0){
                console.log(tbl_name+method+"insert_sum: "+resolve.status)
                model.save_sql_all(DB_NAME, tbl_name, data, resolve_ins =>{
                    // console.log(resolve_ins)
                })
            }else{
                console.log(tbl_name+method+"update")
                model.delete_sql_all(DB_NAME, tbl_name, resolve_del =>{
                    if(resolve_del.status){
                        // console.log(resolve_del)
                        model.save_sql_all(DB_NAME, tbl_name, data, resolve_ins =>{})
                    }
                })
                
            }
        })
    })
}


exports.insert_data_seris = (DB_NAME, tbl_name, data) => {
    return new Promise(cb => {
        model.check_all(DB_NAME, tbl_name, resolve => {
            var method = ": sql: ins_data: ";
            // console.log(resolve)
            if(resolve.data.length == 0){
                console.log(tbl_name+method+"insert_sum: "+resolve.status)
                model.save_sql_all(DB_NAME, tbl_name, data, resolve_ins =>{
                    // console.log(resolve_ins)
                })
            }else{
                console.log(tbl_name+method+"update")
                model.delete_sql_all_seris(DB_NAME, tbl_name, resolve_del =>{
                    if(resolve_del.status){
                        // console.log(resolve_del)
                        model.save_sql_all(DB_NAME, tbl_name, data, resolve_ins =>{})
                    }
                })
                
            }
        })
    })
}

exports.insert_data_seris_tipe = (DB_NAME, tbl_name, tipe, data) => {
    return new Promise(cb => {
        model.check_all_tipe(DB_NAME, tbl_name, tipe, resolve => {
            var method = ": sql: ins_data: ";
            // console.log(resolve)
            if(resolve.data.length == 0){
                console.log(tbl_name+method+"insert_sum: "+resolve.status)
                model.save_sql_all(DB_NAME, tbl_name, data, resolve_ins =>{
                    // console.log(resolve_ins)
                })
            }else{
                console.log(tbl_name+method+"update")
                model.delete_sql_all_seris_tipe(DB_NAME, tbl_name, tipe, resolve_del =>{
                    if(resolve_del.status){
                        // console.log(resolve_del)
                        model.save_sql_all(DB_NAME, tbl_name, data, resolve_ins =>{})
                    }
                })
                
            }
        })
    })
}
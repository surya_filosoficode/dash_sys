const MysqlDbParam = require("../app_dash/MysqlDbParam")

const CUR_ALL = MysqlDbParam.PAJAK_CUR_ALL
const CUR_SUM = MysqlDbParam.PAJAK_CUR_SUM
const SERIS_ALL = MysqlDbParam.PAJAK_SERIS_ALL
const SERIS_SUM = MysqlDbParam.PAJAK_SERIS_SUM

// ==============================================================
// ---------------------------select-----------------------------
// ==============================================================

    exports.select_cur_all = () => {
        return {
            "sql_statement":"SELECT * FROM "+CUR_ALL+" WHERE 1 "
        }
    }

    exports.select_cur_sum = () => {
        return {
            "sql_statement":"SELECT * FROM "+CUR_SUM+" WHERE id= ? "
        }
    }

    exports.select_seris_all = () => {
        return {
            "sql_statement":"SELECT * FROM "+SERIS_ALL+" WHERE periode = ? "
        }
    }

    exports.select_seris_sum = () => {
        return {
            "sql_statement":"SELECT * FROM "+SERIS_SUM+" WHERE periode = ? "
        }
    }


// ==============================================================
// ---------------------------select-----------------------------
// ==============================================================


// ==============================================================
// ---------------------------delete-----------------------------
// ==============================================================
    exports.delete_cur_all = () => {
        return {
            "sql_statement":"DELETE FROM "+CUR_ALL+" WHERE 1"
        }
    }

    exports.delete_cur_sum = () => {
        return {
            "sql_statement":"DELETE FROM "+CUR_SUM+" WHERE 1"
        }
    }

    exports.delete_seris_all = () => {
        return {
            "sql_statement":"DELETE FROM "+SERIS_ALL+" WHERE  periode = ?"
        }
    }

    exports.delete_seris_sum = () => {
        return {
            "sql_statement":"DELETE FROM "+SERIS_SUM+" WHERE  periode = ?"
        }
    }

// ==============================================================
// ---------------------------delete-----------------------------
// ==============================================================

// ==============================================================
// ---------------------------insert-----------------------------
// ==============================================================
    exports.insert_cur_all = () => {
        return {
            "sql_statement":"INSERT INTO "+CUR_ALL+" VALUES ? "
        }
    }

    exports.insert_cur_sum = () => {
        return {
            "sql_statement":"INSERT INTO "+CUR_SUM+" (id, target, realisasi) VALUES ( ? ) "
        }
    }

    exports.insert_seris_all = () => {
        return {
            "sql_statement":"INSERT INTO "+SERIS_ALL+" (id, periode, jenis, target, realisasi) VALUES ? "
        }
    }

    exports.insert_seris_sum = () => {
        return {
            "sql_statement":"INSERT INTO "+SERIS_SUM+" (id, periode, target, realisasi) VALUES ( ? ) "
        }
    }
// ==============================================================
// ---------------------------insert-----------------------------
// ==============================================================


